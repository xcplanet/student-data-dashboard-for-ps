Absencedate,Absencecode,Absencedescription~[tlist_sql;
SELECT to_char(a.att_date,'YYYY-MM-DD') "attdate",
ac.Att_Code "attcode",
CASE ac.Att_Code
WHEN 'U' THEN 'Unexcused'
WHEN 'E' THEN 'Excused'
WHEN 'T' THEN 'Tardy'
WHEN 'M' THEN 'Excused Tardy'
WHEN 'H' THEN 'Hospital'
WHEN 'O' THEN 'Suspended'
WHEN 'X' THEN 'Illness'
ELSE 'Undefined'
END "attcodelong"
FROM Students s
INNER JOIN Attendance a
ON s.id = a.studentid
INNER JOIN Attendance_Code ac
ON ac.ID = a.Attendance_CodeID
WHERE s.id = ~(curstudid)
AND a.Att_Mode_Code = 'ATT_ModeDaily'
AND ac.Att_Code IN ('U','E','X','H','O','T','M')
ORDER BY a.att_date;]
~(attdate),~(attcode),~(attcodelong)[/tlist_sql]
