<!--
  ~ Student Data Dashboard for PowerSchool 7.11.x
  ~ Created Brian Sullivan
  ~
  ~ Version 1.2
  ~ 01/2015
  ~
  ~ Visit https://bitbucket.org/xcplanet/student-data-dashboard-for-ps for updates.
  ~
  ~ HTML is based on generic student and teacher pages from Pearson PowerSchool 7.11.1
  ~
  ~ Graphing code makes use of jQuery and Mike Bostock's excellent d3.js javascript library.
  ~ Many of the graphs used are based on examples found at http://d3js.org/ and https://github.com/mbostock/d3/wiki/Gallery
  -->

    <style>
        body {
            font: 10px sans-serif;
        }
        .day {
            fill: #fff;
            stroke: #ccc;
        }

        .offday{
            fill: #bbb;
        }

        .month {
            fill: none;
            stroke: #000;
            stroke-width: 2px;
        }

        .day.M{fill:rgb(27,158,119);}
        .day.U{fill:rgb(217,95,2)}
        .day.H{fill:rgb(117,112,179)}
        .day.O{fill:rgb(231,41,138)}
        .day.T{fill:rgb(230,171,2)}
        .day.X{fill:rgb(166,118,29)}
        .day.E{fill:rgb(102,102,102)}

        .axis path,
        .axis line {
            fill: none;
            stroke: #000;
            shape-rendering: crispEdges;
        }
        .x.axis path {
            display: none;
        }
        .tooltip {
            margin: 0px;
            background-color: #000;
            color: #fff;
            opacity: 0.8;
            position: relative;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 110%;
            padding: 6px;
            /*padding-top: 0px;*/
            /*padding-bottom: 2px;*/
            border-style: none;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            -moz-box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.3);
            box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.3);
        }
        .ncces-chartdiv {
            display: none;
            margin: 10px;
            /*height: 320px;*/
            /*width: 400px;*/
            color: #000;
            position: relative;
            float: left;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 100%;
            line-height: 120%;
            padding: 20px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
        }

    </style>
