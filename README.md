# Student Data Dashboard for PowerSchool 7.11.x #

Version 1.2, January 2015

Project Repository: https://www.github.com/xcplanet/StudentDataDashboard

A project to provide realtime graphing of selected student test data and other informative metrics without requiring Flash or other browser plugins. The d3.js javascript library is used to render graphic data natively in all modern browsers.

Currently, PowerSchool provides Flash-based Fusion Charts for the built-in data dashboard and there is no built-in visual display of student data. By using javascript, this project provides fast, high-quality, zoomable graphics on all devices, including those that don't support Flash (iOS devices, etc.). It also provides full printing capabilities. The charts have built-in interactivity, with popup tooltips giving more information about data points. In general, hovering over a data point or graph bar will provide more information about it.

++++++++++++++++++++++++++

##DATA SUPPORTED

The code segment for each graph type can be easily commented out in the primary source code (wildcards/student_data_dashboard.txt), as needed. Even if not commented out, a non-existent data set will fail gracefully without any negative effect on the rest of the page.

This version of the project displays the following data.

*ATTENDANCE: fetches a student's attendance data for all enrolled terms from standard PS tables (PS 7.11.1). The wildcards/student_att_csv.txt file should be modified to match your school's daily attendance codes.

*SUBJECT GRADES: This graph plots historical subject-area grades (based on credit type and letter grade). The query can be found in the wildcards/student_subject_scores_csv.txt file and must be modified to work with your school name, credit types and letter grades. The schoolname field is used to filter results so that dual-enrollment courses from outside of our school are ignored. You may want to switch to using schoolid and current school id system variable ~(curschoolid) to make this solution more portable in district use.

*LOG ENTRIES: Fetches a student's standard PS log entries for all enrolled terms.

*GMADE: This graph requires that the GMADE standardized math assessment be set up as a student test called "GMADE Math" in PS. We import the overall stanine score and grade level equivalent score, both as numeric values. The SQL query lives in the wildcards/student_gmade_scores_csv.txt file and relies on the sort order of test score ID numbers to correctly match values (not to be confused with the student test score ID). When we set up the GMADE in PS we added the stanine score first, so it has a lower test score id and, therefore, is pulled first by the query. If your testing shows reversed results you can change the query sort order. This ordering and the name of the test can be modified by editing the SQL query.

*GRADE: This graph requires that the GRADE standardized math assessment be set up as a student test called "GRADE ELA" in PS. We import the overall stanine score and grade level equivalent score, both as numeric values. The SQL query lives in the wildcards/student_grade_scores_csv.txt file and relies on the sort order of test score ID numbers to correctly match values. When we set up the GRADE in PS we added the stanine score first, so it has a lower test score id and, therefore, is pulled first by the query. If your testing shows reversed results you can change the query sort order. This ordering and the name of the test can be modified by editing the SQL query.

*MCAS SCORES: This graph requires that the MCAS (Massachusetts Comprehensive Assessment System) be set up as a student test called "MCAS" in PS. We import the composite scaled scores for ELA, Math and Science, all as numeric values (as opposed to percent or letter). The SQL query lives in the wildcards/student_mcas_scores_csv.txt file and relies on the sort order of test score ID numbers to correctly match values. When we set up the MCAS in PS we added the ELA, Math then Science scores (in that order) so ELA has the lowest test score id and so on. If your testing shows reversed results you can change the query sort order. This ordering and the name of the test can be modified by editing the SQL query.

*TOSCRF: This graph requires that the Test Of Silent Contextual Reading Fluency (TOSCRF) reading assessment be set up as a student test called "TOSCRF" in PS. We import the overall percentile score as a percent value. The SQL query lives in the wildcards/student_toscrf_scores_csv.txt file. The name of the test and other needed fields can be modified by editing the SQL query.

++++++++++++++++++++++++++

##FILE STRUCTURE

The files for this customization are, regrettably, spread throughout the PS web file structure. This is driven by the desire to reduce repetitive code as much as possible.  PS limits user access based on user type and, as such, will only allow logged-in admin users to see pages in the admin section, teachers to see pages in the teacher section, etc.

One way to work with this arrangement is to create wrapper files in the necessary user areas and have them call wildcard files that contain most of the needed logic. This is the approach I chose for both the main graphing logic as well as the asynchronously loaded data files. The advantage of this is that all functional code only lives in one place.

I don't love having most of the logic in one file; it results in a large and unwieldy block of code that's hard to navigate. Ideally, each chart type would be built by separate javascript files that could be easily commented out if not needed.

However, many of us run PowerSchool with an image server and the image server also serves javascript and css files. This arrangement makes it cumbersome to keep multiple javascript files updated so I have opted to use one large logic file that located on the PS server.

Nonetheless, I did create separate css files and they need to be stored on the image server (if you use one). This was for a few reasons, most importantly, the pages use different css files for printing purposes and that's easiest to manage with standalone css files. I didn't want to clutter up the PS web file tree with CSS files when, by convention, they belong in the /images/css directory. As such, CSS files will need to be copied to your image server in the /images/css directory.

++++++++++++++++++++++++++

##INSTALLATION

These pages can be uploaded to your PS server using the PS Administrator web application. Navigate to the Custom Pages area and upload the files to the appropriate folders. You will need to create two specific folders that will contain the tlist_sql file wrappers. Both folders are called "custom_data_files". One is created in admin/students/ and one is created in teachers/studentpages/.

You will need to reset the PowerSchool server before the PowerTeacher student pages navigation dropdown selector will recognize the new student data dashboard page and add it as an option.

+++++++++++++++++++++++++++

##Releases

1.2
Tweaked attendance chart code to allow choosing year sort order. Added flag near top of script for setting sort preference.

1.1
Simplified/normalized graphing code.

1.0
Initial release 12/19/2014